#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <cstring>

// uncomment to enable debug mode
// #define NDEBUG

const unsigned int  WIDTH  = 1280;
const unsigned int  HEIGHT = 720;
const char*         WINDOWTITLE = "VK";

// Validation of layers
const std::vector<const char*> validationLayers{
  "VK_LAYER_KHRONOS_validation" // provided by Khronos Vulkan SDK
};

// We choose to validate if program is compiled in debug mode or not
#ifdef NDEBUG // C++ std. and means "not debug"
  const bool enableValidationLayers = false;
#else
  const bool enableValidationLayers = true;
#endif

VkResult CreateDebugUtilsMessengerEXT(
  VkInstance instance,
  const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
  const VkAllocationCallbacks*              pAllocator,
  VkDebugUtilsMessengerEXT*                 pDebugMessenger
) {

  // returns nullptr if the function couldn't be loaded.
  auto func = (PFN_vkCreateDebugUtilsMessengerEXT)
    vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");

    if (func != nullptr) {
      return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
      return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)
      vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

class HelloTriangleApplication {
public:
  void run() {
    initWindow();
    initVulkan();
    mainLoop();
    cleanup();
  }

private:
  GLFWwindow* p_window;

  VkInstance  instance;
  VkDebugUtilsMessengerEXT debugMessenger;

private:
  void initWindow() {

    if(!glfwInit()){
      throw std::runtime_error("!glfwInit()");
    }

    // Tell GLFW not to use OpenGL (default) context
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    // Disable Window resize option (for now)
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    p_window = glfwCreateWindow(
        WIDTH, HEIGHT, WINDOWTITLE, nullptr, nullptr);

    if(!p_window) {
      throw std::runtime_error("EXIT_FAILURE");
    }
  }

  void initVulkan() {
    createInstance();
    setupDebugMessenger();
  }

  void setupDebugMessenger(){
    if (!enableValidationLayers) return;

    VkDebugUtilsMessengerCreateInfoEXT createInfo;
    populateDebugMessengerCreateInfo(createInfo);

    // Optional, this can instead pass a pointer to the
    // HelloTriangleApplication class (for example).
    createInfo.pUserData = nullptr;

    if(CreateDebugUtilsMessengerEXT(
        instance,
        &createInfo,
        nullptr,
        &debugMessenger) != VK_SUCCESS) {

      throw std::runtime_error("Failed to set up debug messenger!");

    }
  }

  void populateDebugMessengerCreateInfo(
    VkDebugUtilsMessengerCreateInfoEXT& createInfo
  ) {
    createInfo = {};
    createInfo.sType =
      VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;

    // Specifies all the types of severities we want the callback
    // to be called for.
    createInfo.messageSeverity =
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

    // Filter the messages our callback is notified about,
    // this enables all types.
    createInfo.messageType =
      VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT    |
      VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    // specify our callback function
    createInfo.pfnUserCallback = debugCallBack;
  }

  void createInstance() {
    // Validation of layers
    if (enableValidationLayers && !checkValidationLayerSupport()){
      throw std::runtime_error("Validation layers requested, but not available!");
    }

    // struct type, we use this instead of functions with
    // huge amount of parameters.
    VkApplicationInfo appInfo = {};

    // this is optional, however it provides useful information
    // to the driver to optmize for our specific application
    appInfo.sType               = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName    = "Hello World Triangle";
    appInfo.applicationVersion  = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName         = "No Engine";
    appInfo.engineVersion       = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion          = VK_API_VERSION_1_0;

    // Another struct, where we fill the information Vulkan needs
    // in order to create the instance
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo     = &appInfo;

    // use GLFW built-in function that returns the extension(s) needed
    // to pass to the VkInstaceCreateInfo Struct
    auto extensions = getRequiredExtensions();

    createInfo.enabledExtensionCount    =
      static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames  = extensions.data();

    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    if (enableValidationLayers) {
      createInfo.enabledLayerCount    = static_cast<uint32_t>(validationLayers.size());
      createInfo.ppEnabledLayerNames  = validationLayers.data();

      populateDebugMessengerCreateInfo(debugCreateInfo);
      createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) & debugCreateInfo;

    } else {
      createInfo.enabledLayerCount = 0;
      createInfo.pNext             =  nullptr;
    }

    // All done, create the instance.
    VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

    // Note that this may return VK_ERROR_EXTENSION_NOT_PRESENT, to
    // prevent that we need specify the extensions we require.
    if (result != VK_SUCCESS) {
      throw std::runtime_error("Failed to create instance!");
    }

  }

  bool checkValidationLayerSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const char* layerName : validationLayers){
      bool layerFound = false;

      for(const auto& layerProperties : availableLayers) {
        if (strcmp(layerName, layerProperties.layerName) == 0) {
          layerFound = true;
          break;
        }
      }

      if (!layerFound) {
        return false;
      }
    }

    return true;
  }

  // returns the required list of extensions based on wheter validation
  // layers are enabled or not
  std::vector<const char*> getRequiredExtensions(){
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions =
      glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*>
      extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if(enableValidationLayers) {
      extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return extensions;
  }

  void mainLoop() {
    while(!glfwWindowShouldClose(p_window)){
      glfwPollEvents();
    }
  }

  void cleanup() {
    if (enableValidationLayers) {
      DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
    }
    vkDestroyInstance(instance, nullptr);
    glfwDestroyWindow(p_window);
    glfwTerminate();
  }

  // debug callback function (VKAPI_ATTR and VKAPI_CALL makes sure
  // Vulkan to call it)
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallBack (
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallBackData,
    void * pUserData
  ){
    std::cerr << "Validation layer: " << pCallBackData->pMessage << std::endl;
    return VK_FALSE;
  }

};

int main() {
    HelloTriangleApplication app;

    try {
        app.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

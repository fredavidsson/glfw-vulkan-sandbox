LDLIBS := -lglfw3 -ldl -lX11 -lpthread -lvulkan -lm
FLAGS  := -std=c++11
INC    := ./include/


main: main.o
	g++ -o main main.o $(LDLIBS) -I $(INC) $(FLAGS)


main.o: main.cpp
	g++ -c main.cpp $(LDLIBS) $(FLAGS)
